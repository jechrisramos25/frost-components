let ratingItems = Array.from(document.querySelectorAll('.rating-item'));

let [item1, item2, item3, item4, item5] = ratingItems;

item1.addEventListener("click", () => {
    if(item2.classList.contains("checked") || item3.classList.contains("checked") || item4.classList.contains("checked") || item5.classList.contains("checked")){
        item2.classList.remove("checked");
        item3.classList.remove("checked");
        item4.classList.remove("checked");
        item5.classList.remove("checked");
    }
    item1.classList.add("checked");
});

item2.addEventListener("click", () => {
    if(item1.classList.contains("checked") || item3.classList.contains("checked") || item4.classList.contains("checked") || item5.classList.contains("checked")){
        item1.classList.remove("checked");
        item3.classList.remove("checked");
        item4.classList.remove("checked");
        item5.classList.remove("checked");
    }
    item2.classList.add("checked");
});

item3.addEventListener("click", () => {
    if(item1.classList.contains("checked") || item2.classList.contains("checked") || item4.classList.contains("checked") || item5.classList.contains("checked")){
        item1.classList.remove("checked");
        item2.classList.remove("checked");
        item4.classList.remove("checked");
        item5.classList.remove("checked");
    }
    item3.classList.add("checked");
});

item4.addEventListener("click", () => {
    if(item1.classList.contains("checked") || item2.classList.contains("checked") || item3.classList.contains("checked") || item5.classList.contains("checked")){
        item1.classList.remove("checked");
        item2.classList.remove("checked");
        item3.classList.remove("checked");
        item5.classList.remove("checked");
    }
    item4.classList.add("checked");
});

item5.addEventListener("click", () => {
    if(item1.classList.contains("checked") || item2.classList.contains("checked") || item3.classList.contains("checked") || item4.classList.contains("checked")){
        item1.classList.remove("checked");
        item2.classList.remove("checked");
        item3.classList.remove("checked");
        item4.classList.remove("checked");
    }
    item5.classList.add("checked");
    
});